﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.increment = New System.Windows.Forms.Button()
        Me.decrement = New System.Windows.Forms.Button()
        Me.RAZ = New System.Windows.Forms.Button()
        Me.ValeurLabel = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'increment
        '
        Me.increment.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.25!)
        Me.increment.Location = New System.Drawing.Point(69, 189)
        Me.increment.Name = "increment"
        Me.increment.Size = New System.Drawing.Size(214, 50)
        Me.increment.TabIndex = 0
        Me.increment.Text = "+"
        Me.increment.UseVisualStyleBackColor = True
        '
        'decrement
        '
        Me.decrement.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.decrement.Location = New System.Drawing.Point(509, 189)
        Me.decrement.Name = "decrement"
        Me.decrement.Size = New System.Drawing.Size(214, 50)
        Me.decrement.TabIndex = 1
        Me.decrement.Text = "-"
        Me.decrement.UseVisualStyleBackColor = True
        '
        'RAZ
        '
        Me.RAZ.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.25!)
        Me.RAZ.Location = New System.Drawing.Point(289, 280)
        Me.RAZ.Name = "RAZ"
        Me.RAZ.Size = New System.Drawing.Size(214, 50)
        Me.RAZ.TabIndex = 2
        Me.RAZ.Text = "RAZ"
        Me.RAZ.UseVisualStyleBackColor = True
        '
        'ValeurLabel
        '
        Me.ValeurLabel.AutoSize = True
        Me.ValeurLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ValeurLabel.Location = New System.Drawing.Point(374, 188)
        Me.ValeurLabel.Name = "ValeurLabel"
        Me.ValeurLabel.Size = New System.Drawing.Size(39, 42)
        Me.ValeurLabel.TabIndex = 3
        Me.ValeurLabel.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(374, 172)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Total"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ValeurLabel)
        Me.Controls.Add(Me.RAZ)
        Me.Controls.Add(Me.decrement)
        Me.Controls.Add(Me.increment)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents increment As Button
    Friend WithEvents decrement As Button
    Friend WithEvents RAZ As Button
    Friend WithEvents ValeurLabel As Label
    Friend WithEvents Label2 As Label
End Class
